.section .init
.globl _start
_start:

b main

.section .data

tulosclear:
.ascii "pisteet=  "

tulos:
.ascii "pisteet=%d"

tulosEnd:
.rept 12				// varaa tilaa, ettei ylikirjoita seuraavia osoitteita
.int 0
.endr


go:
.ascii "GAME OVER"


.align 4
Piste:
.int 0			// #0 X
.int 0			// #4 Y
.int 0			// #8 rand 

.align 4
pituus:
.int 0

.align 4
Snakex:
.rept 200
.int 0
.endr

.align 4
Snakey:
.rept 200
.int 0
.endr

.section .text

main:
 
mov sp,#0x9000

mov r0,#1024					//  N�YT�N ASETUKSET
mov r1,#768
mov r2,#16					// 16 high color
bl InitialiseFrameBuffer

teq r0,#0					//error, kun InitialiseFrameBuffer palauttaa 0 -> led p��lle
bne noError$

mov r0,#16
mov r1,#1
bl SetGpioFunction
mov r0,#16
mov r1,#0
bl SetGpio

error$:
b error$

noError$:
fbInfoAddr .req r4
mov fbInfoAddr,r0

bl SetGraphicsAddress

.unreq fbInfoAddr 

bl UsbInitialise


//								*******************
// 								**** TKT SNAKE ****
//								*******************

//				CREDITS:
//				Director:			Juha Salminen
//				Executive Producer:	Juha Salminen
//				Project Leader:		Juha Salminen
//				Programming:		Juha Salminen
//				Graphics:			Juha Salminen
//				


//							*** Alkun�ytt� ***

bl Aloita

Sx .req r4						// snake koord. address
Sy .req r8
ldr r4,=Snakex
ldr r8,=Snakey

mov r6,#250						// Snaken l�ht�piste
mov r9,#250

								//Snaken koordinaatit
x .req r6 						//r6 = X
y .req r9						//r9 = Y

ldr r2,=pituus					//pituus
mov r0,#8
str r0,[r2]

//alkuarvo pisteelle
ldr r2,=Piste
mov r0,#100
mov r1,#100
str r0,[r2]				//x
str r1,[r2,#4]				//y
//str r1,[r2,#8]			//rand

/* ldr r0,[r2,#4]	
bl Random
str r0,[r2]
bl Random
str r0,[r2,#4]
//Randomisoi koordinaatit y:n perusteella
//bl RandC
ldr r0,[r2]
ldr r1,[r2,#4]
bl DrawPixel */


//							***clear screen***
//								-tyhjent�� pelialueen
bl cls

					
//					*** Sein�t ***
					

bl seinat				// -> funktioksi


//						TULOS
/* ldr r0,=pituus
ldr r3,[r0]				//argumentti
//sub r3, #9 */

mov r3,#0
ldr r0,=tulos
ldr r2,=tulosEnd
mov r1,#10
//mov r1,#tulosEnd-tulos		// stringin pituus
bl FormatString				// muuta num. stringiksi
ldr r0,=tulosEnd
mov r1,#10
mov r2,#548				//r1=str. pituus, r0=str.osoite, r2,r3=str. koord. 
mov r3,#90	
bl DrawString


mov r0,#0xA000	//pun
bl SetForeColour

// alkuarvot koordinaateille
ldr r2,=Piste
bl RandC
ldr r0,[r2]
ldr r1,[r2,#4]
bl DrawPixel

//piste
/* ldr r2,=Piste
ldr r0,[r2]
ldr r1,[r2,#4]
bl DrawPixel */

/* mov r5,#36
mov r7,#36

initloop$:				// piirt�� madon
mov r7,r5
sub r7,#4
str r6,[Sx,r7]				// tallenna xy seuraavaan osoitteeseen
str r9,[Sy,r7]				// r5 = offset
add r6,#1				// x++
ldr r0,[Sx,r5]				// lataa xy ja piirr�
ldr r1,[Sy,r5] 
bl DrawPixel
sub r5,#4				// offset++
teq r5,#0				//[pituus,lsl #2]jos r5 = madon pituus, r5 -> 4, palaa alkuun
bne initloop$ */
add r6,#1
//mov r5,#0		
mov r10,#4				//suunta

//							***************
//							** MAIN LOOP **
//							***************
loop2$:

mov r0,#0xA000	//pun.
bl SetForeColour

//***test
/* ldr r2,=Piste
bl RandC
ldr r0,[r2]
ldr r1,[r2,#4]
bl DrawPixel */
/* randloop1$:
bl Random
cmp r0,#500
bhi randloop1$

str r0,[r2]
randloop2$:
bl Random
str r0,[r2,#4]
cmp r0,#500					//uutta randomia, kunnes <500
bhi randloop2$ */
//***clear screen				->funktioksi
//bl cls
/* mov r7,r5
add r7,#4 */


//							***STACK***	
// pituus*4
ldr r2,=pituus
ldr r0,[r2]

mov r5,r0,lsl #2
mov r7,r5					//pituuden mukaan
sub r7,#4					//OK!

stackloop$:					//r2 = stack register
ldr r2,[Sx,r7]					// tallenna xy seuraavaan osoitteeseen
str r2,[Sx,r5]
ldr r2,[Sy,r7]
str r2,[Sy,r5]
 
sub r7,#4
sub r5,#4		
teq r5,#0
bne stackloop$					//luuppaa pituuden mukaan


str r6,[Sx]					//vanhin koord. viimeiseksi
str r9,[Sy] 
						//uusi on nyt #0:ssa
							
/* ldr r0,[Sx,r5]				// lataa xy ja piirr�
ldr r1,[Sy,r5] 
bl DrawPixel */

//Piirr� piste
/* mov r0,#0xA000				//pun.
bl SetForeColour
ldr r2,=Piste
ldr r0,[r2]
ldr r1,[r2,#4]
bl DrawPixel
 */
mov r7,#0

//										-----
//								*** PIIRR� N�YTT� ***
//										-----

draw$:						
						// r7 luuppaa koordinaattien offsetit l�pi
							
ldr r0,[Sx,r7]					// lataa xy ja piirr�
ldr r1,[Sy,r7] 
bl DrawPixel

ldr r2,=pituus
ldr r0,[r2]
mov r5,r0,lsl #2				//r5: pituus

add r7,#4
teq r7,r5
bne draw$ 					// looppaa, kunnes toiseksi viimeinen osoite


mov r7,r5
mov r0,#0x0000					//Vanhin takaisin taustav�riin
bl SetForeColour

ldr r0,[Sx,r7]					// lataa xy ja piirr�
ldr r1,[Sy,r7] 
bl DrawPixel

//mov r1,#4
//	***n�pp�imist� ohjaa suunnan***


bl KeyboardUpdate				// P�ivitt�� n�ppiksen
bl KeyboardGetAddress				// osoite argumentiksi KeyboardGetKeyDownille, r0
mov r1,#0
bl KeyboardGetKeyDown				// antaa painetun napin argumentin r1 perusteella
						// r1 on j�rjestysnumero, #0 = ensimm�inen painettu n�pp�in 
teq r0,#4					//A
moveq r10,#1
teq r0,#22					//S
moveq r10,#2
teq r0,#26					//W
moveq r10,#3
teq r0,#7					//D 
moveq r10,#4

//						**** DEBUG HUIJAUSKOODI ****
teq r0,#19					//P
ldreq r2,=pituus
addeq r0,#32
streq r0,[r2]
//							*** ***

teq r10,#4
addeq r6,#1		//x++
teq r10,#2
addeq r9,#1		//y++
teq r10,#1
subeq r6,#1		//x--
teq r10,#3
subeq r9,#1		//y--



add r5,#4
teq r5,#36		//[pituus,lsl #2]jos r5 = madon pituus, r5 -> 4, palaa alkuun
moveq r5,#4

//**************DEBUG LED********************
 mov r0,#16
mov r1,#1
bl SetGpioFunction
mov r0,#16
mov r1,#0
bl SetGpio 
//*******************************************


//						T�RM�YKSENTARKISTUS

//					   ----
//					***MATO***
//					   ----
mov r7,#0
mato$:						
						// r7 luuppaa koordinaattien offsetit, Sx,Sy : koordinaattien osoitteet						
ldr r0,[Sx,r7]					// lataa xy ja vertaa koordinaatteihin
ldr r1,[Sy,r7] 
						//r6:X r9:Y
teq r0,r6
teqeq r1,r9
beq GAMEOVER$					//jos x on sama -> jos y on sama -> gameover

ldr r2,=pituus
ldr r0,[r2]
mov r5,r0,lsl #2				//r5: pituus

add r7,#4
cmp r7,r5					// luuppaa pituuden verran
ble mato$ 	 
break$:
//						---
//					***REUNAT***
//						---
teq r6,#500
beq GAMEOVER$
teq r9,#500
beq GAMEOVER$
teq r6,#0
beq GAMEOVER$
teq r9,#0
beq GAMEOVER$

//						---
//					***PISTE***
//						---
ldr r2,=Piste
ldr r1,[r2]
ldr r0,[r2,#4] 
//					***		X
teq r1,r6
bne loop3$
//					***		Y
teq r0,r9
bne loop3$
//					***olet t�ss�, jos koordinaatit samat
//points++
ldr r2,=pituus
ldr r0,[r2]
add r0,#4
str r0,[r2]

//					***Generoi uus piste***
//v�ri
mov r0,#0xA000	//pun.
bl SetForeColour

ldr r2,=Piste
ldr r1,[r2]
ldr r0,[r2,#4] 
bl RandC
ldr r0,[r2]
ldr r1,[r2,#4]
bl DrawPixel 
//					***piirr� tulos
//					pistealueen tyhjennys

bl clspoints

mov r0,#0x0A00	//vih.
bl SetForeColour

ldr r0,=pituus
ldr r3,[r0]	
//sub r3,#4				// Pituus kasvaa  NELJ�LL� -> LSR #2  
lsr r3,#2				// V�henn� alkupituus :2
sub r3,#2
ldr r0,=tulos
ldr r2,=tulosEnd
mov r1,#10
cmp r3,#10
movle r1,#10
					// stringin pituus
bl FormatString	
ldr r0,=tulosEnd
mov r1,#10
cmp r3,#10
movle r1,#9
mov r2,#548
mov r3,#90				// muuta num. stringiksi
bl DrawString

loop3$:
ldr r0,=12000
bl Wait


b loop2$

GAMEOVER$:
//				***Tyhjenn� kentt�
bl cls
mov r0,#0x0A00	//vih
bl SetForeColour
ldr r0,=go
mov r1,#9
mov r2,#200
mov r3,#250
bl DrawString

//				uusi piste
mov r0,#0xA000	//pun
bl SetForeColour
ldr r2,=Piste
bl RandC
ldr r0,[r2]
ldr r1,[r2,#4]
bl DrawPixel 

//				nollaa pituus
ldr r2,=pituus
mov r0,#8			//alkupituus
str r0,[r2]



ldr r2,=tulosEnd		//pisteet=xy -> poista y,  #36
ldr r1,=tulosclear
ldr r0,[r1]			//aseta nollaksi???
str r0,[r2]


/* str r0,[r2,#36]
str r0,[r2,#32]
str r0,[r2,#40] */

//nollaa tulos			!!!!!!!!!!!!!!!!!!!!
bl clspoints
mov r3,#0
ldr r0,=tulos
ldr r2,=tulosEnd
mov r1,#10
cmp r3,#10
movle r1,#10			// stringin pituus
//mov r1,#tulosEnd-tulos
bl FormatString			// muuta num. stringiksi, -> tulosEnd
ldr r0,=tulosEnd
mov r1,#10
cmp r3,#10
movle r1,#9
mov r2,#548
mov r3,#90	
bl DrawString

//wait
ldr r0,=900000
bl Wait
mov r6,#255
mov r9,#255

b loop2$

