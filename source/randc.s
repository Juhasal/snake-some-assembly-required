.globl RandC
RandC:				//r2:osoite
push {r4,r5,lr}

ldr r0,[r2,#4]	

randloop1$:
bl Random
mov r4,r0
lsr r4,#22
cmp r4,#500
bhi randloop1$
str r4,[r2]

randloop2$:
bl Random
mov r5,r0
lsr r5,#22
cmp r5,#500					//uutta randomii, kunnes <500
bhi randloop2$
str r5,[r2,#4]

pop {r4,r5,pc}