.section .text

.globl cls
cls:
push {r4,r5,r6,lr}

mov r0,#0x0000
bl SetForeColour

mov r4,#0
mov r5,#1

loop$:
mov r0,r4
mov r1,r5
bl DrawPixel
add r4,#1

teq r4,#500
moveq r4,#1
addeq r5,#1

ldr r6,=499
cmp r5,r6
ble loop$

pop {r4,r5,r6,pc}

//Pistealueen tyhjennys

.globl clspoints
clspoints:
push {r4,r5,r6,lr}

mov r0,#0x0000
bl SetForeColour

mov r4,#548
mov r5,#90

pointloop$:
mov r0,r4
mov r1,r5
bl DrawPixel
add r4,#1

teq r4,#720
moveq r4,#548
addeq r5,#1

ldr r6,=110
cmp r5,r6
ble pointloop$

pop {r4,r5,r6,pc}