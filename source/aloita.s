//Alkuscreen
.globl Aloita
Aloita:

push {lr}

mov r0,#0x0A00	//vih.
bl SetForeColour
ldr r0,=900000
bl Wait

//TKTSNAKE : ascii: 84;75;84;83;78;65;75;69
ldr r0,=900000
bl Wait
mov r0,#84
mov r1,#150
mov r2,#250
bl DrawCharacter
ldr r0,=500000
bl Wait
mov r0,#75
mov r1,#160
mov r2,#252
bl DrawCharacter
ldr r0,=500000
bl Wait
mov r0,#84
mov r1,#170
mov r2,#250
bl DrawCharacter
ldr r0,=500000
bl Wait
mov r0,#83
mov r1,#180
mov r2,#252
bl DrawCharacter
ldr r0,=500000
bl Wait
mov r0,#78
mov r1,#190
mov r2,#250
bl DrawCharacter
ldr r0,=500000
bl Wait
mov r0,#65
mov r1,#200
mov r2,#252
bl DrawCharacter
ldr r0,=500000
bl Wait
mov r0,#75
mov r1,#210
mov r2,#250
bl DrawCharacter
ldr r0,=500000
bl Wait
mov r0,#69
mov r1,#220
mov r2,#252
bl DrawCharacter
ldr r0,=900000
bl Wait

pop {pc}
