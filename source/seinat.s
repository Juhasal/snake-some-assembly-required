.section .data

teksti:
.ascii "TKT-SNAKE"

wasd:
.ascii "WASD-napit ohjaa matoa"

piedition:
.ascii "PI - Edition"

.section .text
//Piirr� pelialue
.globl seinat
seinat:

push {lr}

//boxi
mov r0,#0x0A00	//vih
bl SetForeColour
mov r0,#0
mov r1,#0
mov r2,#500
mov r3,#0
bl DrawLine
mov r0,#0
mov r1,#0
mov r2,#0
mov r3,#500
bl DrawLine
mov r0,#500
mov r1,#0
mov r2,#500
mov r3,#500
bl DrawLine
mov r0,#0
mov r1,#500
mov r2,#500
mov r3,#500
bl DrawLine



//PI_EDITION
mov r0,#0x0A00	//pun
bl SetForeColour

ldr r0,=piedition
mov r1,#13
mov r2,#548
mov r3,#130
bl DrawString

//toinen boxi
mov r0,#520
mov r1,#0
mov r2,#800
mov r3,#0
bl DrawLine

mov r0,#520
mov r1,#0
mov r2,#520
mov r3,#150
bl DrawLine

mov r0,#520
mov r1,#150
mov r2,#800
mov r3,#150
bl DrawLine

mov r0,#800
mov r1,#0
mov r2,#800
mov r3,#150
bl DrawLine


//v�ri
mov r0,#0x0A00	//vih
bl SetForeColour
//TKT-SNAKE
ldr r0,=teksti
mov r1,#9
mov r2,#548
mov r3,#10
bl DrawString
//WASD
ldr r0,=wasd
mov r1,#22
mov r2,#548
mov r3,#50
bl DrawString

pop {pc}


