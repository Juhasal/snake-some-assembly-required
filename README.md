# README #

### SNAKE (some assembly required) ###

As a substitute to pre-assignments in Computer Engineering course, I followed the Cambridge University's "Baking Pi – Operating Systems Development" project tutorial by  Alex Chadwick to develop an operating system in Assembly language. (Link to the course: https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/os/) 

As a final assignment, I programmed a snake game on top of that operating system. The whole project runs on Raspberry Pi from an SD-card. Here's the source code for it.

